ENGLISH
=======
The goal of the TRIA SISTEMA OPERATIU program is to show anyone who has it questions about
which operating system(s) to put on your computer(s):

A) that there are FREE GNU / Linux Operating Systems in Catalan from the beginning, free
and legal FOREVER and that allow you to do EVERYTHING you want to do with your computer, 
today and forever.

B) that Linux does not force you to spend on improving your computer, but makes better use
of what you already have.

and most importantly:

C) TRIA S.O. compare the years of Operating Systems (free and proprietary) you could put
in and it helps you to see that you can have more software on your computer if you choose
Linux and even if you want (there is still MANY PEOPLE reluctant to leave Windows) tells you
how you can do this to have Windows and Linux on a single computer.

My goal is to help the people who hesitate to put it on their PC (s) to see that if they
don't have a VERY POWERFUL AND EXPENSIVE computer, nowhere of spending money to use only 
Windows 10, your computer would work better with a modern Linux (or with a modern Linux
and a not so modern Windows)


CATALÀ
======

El objectiu del programa TRIA SISTEMA OPERATIU es fer veure a qualsevol persona que tingui 
dubtes de quin sistema operatiu (o sistemes) posar en el(s) seu(s) ordinador(s) el següent:

A) que hi ha Sistemes Operatius GNU/Linux LLIURES en català des del inici, gratuïts i legals PER SEMPRE
i que et permeten fer TOT allò que vulguis fer amb el teu ordinador, avui i sempre.

B) que Linux no t'obliga a gastar en millorar el teu ordinador, sinó que aprofita MILLOR el que ja tens.

i el mes important:

C) TRIA S.O. compara els anys dels Sistemes Operatius (lliures i privatius) que podries posar i t'ajuda 
a veure que pots tenir un programari mes teu ordinador si tries Linux i fins i tot, si vols (hi ha encara 
MOLTA GENT reticent a abandonar Windows) et diu com pots fer-ho per tenir Windows i Linux a un sol ordinador.

El meu objectiu es ajudar a la gent que dubta de que posar al seu(s) PC(s) a veure que, si no tenen un
ordinador MOLT POTENT I CAR, enlloc de gastar diners per utilitzar només Windows 10, el seu ordinador 
treballaria millor amb un Linux modern (o amb un Linux modern i un Windows no tant modern).


ESPAÑOL
=======

El objetivo del programa ELIGE SISTEMA OPERATIVO se hacer ver a cualquier persona que tenga
dudas de qué sistema operativo (o sistemas) poner en el (los) su (s) computadora (s) lo siguiente:

A) que hay Sistemas Operativos GNU / Linux LIBRES en catalán desde el inicio, gratuitos y legales PARA SIEMPRE
y que te permiten hacer TODO lo que quieras hacer con tu ordenador, hoy y siempre.

B) que Linux no te obliga a gastar en mejorar tu ordenador, sino que aprovecha MEJOR lo que ya tienes.

y lo más importante:

C) ELIGE S.O. compara los años los Sistemas Operativos (libres y privativos) que podrías poner y te ayuda
a ver que puedes tener un software mes tu ordenador si eliges Linux e incluso, si quieres (hay todavía
MUCHA GENTE reticente a abandonar Windows) te dice cómo puedes hacerlo para tener Windows y Linux en un 
solo ordenador.

Mi objetivo es ayudar a la gente que duda de que poner a su (s) PC (s) a ver que, si no tienen un ordenador
MUY POTENTE Y CAR, en lugar de gastar dinero para utilizar sólo Windows 10, su ordenador trabajaría mejor
con un Linux moderno (o con un Linux moderno y un Windows no tanto moderno).


FRANÇAISE
=========

L'objectif du programme TRIA SISTEMA OPERATIU est de montrer à quiconque en est atteint questions sur
le ou les systèmes d'exploitation à installer sur votre ou vos ordinateurs:

A) qu'il existe des systèmes d'exploitation GNU / Linux GRATUITS en catalan depuis le début, gratuits
et légaux POUR TOUJOURS et qui vous permettent de faire TOUT ce que vous voulez faire avec votre ordinateur, 
aujourd'hui et pour toujours.

B) que Linux ne vous oblige pas à dépenser pour améliorer votre ordinateur, mais utilise mieux ce que vous
avez déjà.

et, surtout:

C) TRIA S.O. comparer les années de systèmes d'exploitation (gratuits et propriétaires) que vous pourriez
installer et cela vous aide pour voir que vous pouvez avoir plus de logiciels sur votre ordinateur si vous
choisissez Linux et même si vous le souhaitez (il y a encore BEAUCOUP DE PERSONNES réticents à quitter Windows)
vous explique comment procéder pour avoir Windows et Linux sur un seul ordinateur.

Mon objectif est de le publier cet été (le donner) pour aider le les gens qui hésitent à le mettre sur leur
PC(s) pour voir que s'ils n'ont pas un ordinateur TRÈS PUISSANT ET CHER, nulle part de dépenser de l'argent
pour utiliser uniquement Windows 10, votre ordinateur fonctionnerait mieux avec un Linux moderne (ou avec un
Linux moderne et un Windows pas si moderne).
