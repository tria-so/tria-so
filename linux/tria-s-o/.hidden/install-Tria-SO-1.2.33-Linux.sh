#!/bin/bash

# Un cop s'ha descomprimit el arxiu install-TriaSO-1.2.33-Linux.zip (amb
# TriaSO.gambas, unetbootin-linux-702.bin i unetbootin-linux64-702.bin)
# li donem permisos d'execució als 3 arxius i canviem el nom dels binaris
# unetbootin per permetre que Tria S.O. 1.2.33 obri el de 64 bits quan
# es faci servir cpu de 64 bits i si no, el unetbootin de 32 bits.

clear

chmod +x TriaSO.gambas
chmod +x unetbootin-linux*
mv unetbootin-linux-702.bin unetbootin32
mv unetbootin-linux64-702.bin unetbootin

echo 'To be able to use unetboontin to burn a iso file to USB'
echo 'now you will have to allow the installer to copy unetbootin'
echo 'to the right path (/usr/bin/ requires sudo password).'
sudo cp unetboot* /usr/bin

clear

echo 'Tria S.O. 1.2.33 for Linux already installed  !'
echo 'Create a shorcut of the file Tria-SO.gambas to your desktop'
echo 'if you want to run the program from your desktop.'
