    Public Enum HRESULT As Integer
        S_OK = 0
        S_FALSE = 1
        E_NOINTERFACE = &H80004002
        E_NOTIMPL = &H80004001
        E_FAIL = &H80004005
        E_UNEXPECTED = &H8000FFFF
    End Enum

    <ComImport>
    <Guid("7b7166ec-21c7-44ae-b21a-c9ae321ae369")>
    <InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>
    Interface IDXGIFactory
        Inherits IDXGIObject

        Overloads Function SetPrivateData(ByRef Name As Guid, ByVal DataSize As UInteger, ByVal pData As IntPtr) As HRESULT
        Overloads Function SetPrivateDataInterface(ByRef Name As Guid, ByVal pUnknown As IntPtr) As HRESULT
        Overloads Function GetPrivateData(ByRef Name As Guid, <Out> ByRef pDataSize As UInteger, <Out> ByRef pData As IntPtr) As HRESULT
        Overloads Function GetParent(ByRef riid As Guid, <Out> ByRef ppParent As IntPtr) As HRESULT
        <PreserveSig>
        Function EnumAdapters(ByVal Adapter As UInteger, <Out> ByRef ppAdapter As IDXGIAdapter) As HRESULT
        Function MakeWindowAssociation(ByVal WindowHandle As IntPtr, ByVal Flags As UInteger) As HRESULT
        Function GetWindowAssociation(<Out> ByRef pWindowHandle As IntPtr) As HRESULT
        Function CreateSwapChain(ByVal pDevice As IntPtr, ByVal pDesc As DXGI_SWAP_CHAIN_DESC, <Out> ByRef ppSwapChain As IDXGISwapChain) As HRESULT
        Function CreateSoftwareAdapter(ByVal [Module] As IntPtr, <Out> ByRef ppAdapter As IDXGIAdapter) As HRESULT
    End Interface

    <ComImport>
    <Guid("770aae78-f26f-4dba-a829-253c83d1b387")>
    <InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>
    Interface IDXGIFactory1
        Inherits IDXGIFactory

        Overloads Function SetPrivateData(ByRef Name As Guid, ByVal DataSize As UInteger, ByVal pData As IntPtr) As HRESULT
        Overloads Function SetPrivateDataInterface(ByRef Name As Guid, ByVal pUnknown As IntPtr) As HRESULT
        Overloads Function GetPrivateData(ByRef Name As Guid, <Out> ByRef pDataSize As UInteger, <Out> ByRef pData As IntPtr) As HRESULT
        Overloads Function GetParent(ByRef riid As Guid, <Out> ByRef ppParent As IntPtr) As HRESULT
        Overloads Function EnumAdapters(ByVal Adapter As UInteger, <Out> ByRef ppAdapter As IDXGIAdapter) As HRESULT
        Overloads Function MakeWindowAssociation(ByVal WindowHandle As IntPtr, ByVal Flags As UInteger) As HRESULT
        Overloads Function GetWindowAssociation(<Out> ByRef pWindowHandle As IntPtr) As HRESULT
        Overloads Function CreateSwapChain(ByVal pDevice As IntPtr, ByVal pDesc As DXGI_SWAP_CHAIN_DESC, <Out> ByRef ppSwapChain As IDXGISwapChain) As HRESULT
        Overloads Function CreateSoftwareAdapter(ByVal [Module] As IntPtr, <Out> ByRef ppAdapter As IDXGIAdapter) As HRESULT
        <PreserveSig>
        Function EnumAdapters1(ByVal Adapter As UInteger, <Out> ByRef ppAdapter As IDXGIAdapter1) As HRESULT
        Function IsCurrent() As Boolean
    End Interface

    <ComImport>
    <Guid("29038f61-3839-4626-91fd-086879011a05")>
    <InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>
    Interface IDXGIAdapter1
        Inherits IDXGIAdapter

        Overloads Function SetPrivateData(ByRef Name As Guid, ByVal DataSize As UInteger, ByVal pData As IntPtr) As HRESULT
        Overloads Function SetPrivateDataInterface(ByRef Name As Guid, ByVal pUnknown As IntPtr) As HRESULT
        Overloads Function GetPrivateData(ByRef Name As Guid, <Out> ByRef pDataSize As UInteger, <Out> ByRef pData As IntPtr) As HRESULT
        Overloads Function GetParent(ByRef riid As Guid, <Out> ByRef ppParent As IntPtr) As HRESULT
        <PreserveSig>
        Overloads Function EnumOutputs(ByVal Output As UInteger, ByRef ppOutput As IDXGIOutput) As HRESULT
        Overloads Function GetDesc(<Out> ByRef pDesc As DXGI_ADAPTER_DESC) As HRESULT
        Overloads Function CheckInterfaceSupport(ByRef InterfaceName As Guid, <Out> ByRef pUMDVersion As LARGE_INTEGER) As HRESULT
        Function GetDesc1(<Out> ByRef pDesc As DXGI_ADAPTER_DESC1) As HRESULT
    End Interface

    <StructLayout(LayoutKind.Sequential)>
    Public Structure DXGI_SWAP_CHAIN_DESC
        Public BufferDesc As DXGI_MODE_DESC
        Public SampleDesc As DXGI_SAMPLE_DESC
        Public BufferUsage As UInteger
        Public BufferCount As UInteger
        Public OutputWindow As IntPtr
        Public Windowed As Boolean
        Public SwapEffect As DXGI_SWAP_EFFECT
        Public Flags As UInteger
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure DXGI_MODE_DESC
        Public Width As UInteger
        Public Height As UInteger
        Public RefreshRate As DXGI_RATIONAL
        Public Format As DXGI_FORMAT
        Public ScanlineOrdering As DXGI_MODE_SCANLINE_ORDER
        Public Scaling As DXGI_MODE_SCALING
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure DXGI_SAMPLE_DESC
        Public Count As UInteger
        Public Quality As UInteger
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure DXGI_RATIONAL
        Public Numerator As UInteger
        Public Denominator As UInteger
    End Structure

    Public Enum DXGI_SWAP_EFFECT
        DXGI_SWAP_EFFECT_DISCARD = 0
        DXGI_SWAP_EFFECT_SEQUENTIAL = 1
        DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL = 3
        DXGI_SWAP_EFFECT_FLIP_DISCARD = 4
    End Enum

    Public Enum DXGI_FORMAT
        DXGI_FORMAT_UNKNOWN = 0
        DXGI_FORMAT_R32G32B32A32_TYPELESS = 1
        DXGI_FORMAT_R32G32B32A32_FLOAT = 2
        DXGI_FORMAT_R32G32B32A32_UINT = 3
        DXGI_FORMAT_R32G32B32A32_SINT = 4
        DXGI_FORMAT_R32G32B32_TYPELESS = 5
        DXGI_FORMAT_R32G32B32_FLOAT = 6
        DXGI_FORMAT_R32G32B32_UINT = 7
        DXGI_FORMAT_R32G32B32_SINT = 8
        DXGI_FORMAT_R16G16B16A16_TYPELESS = 9
        DXGI_FORMAT_R16G16B16A16_FLOAT = 10
        DXGI_FORMAT_R16G16B16A16_UNORM = 11
        DXGI_FORMAT_R16G16B16A16_UINT = 12
        DXGI_FORMAT_R16G16B16A16_SNORM = 13
        DXGI_FORMAT_R16G16B16A16_SINT = 14
        DXGI_FORMAT_R32G32_TYPELESS = 15
        DXGI_FORMAT_R32G32_FLOAT = 16
        DXGI_FORMAT_R32G32_UINT = 17
        DXGI_FORMAT_R32G32_SINT = 18
        DXGI_FORMAT_R32G8X24_TYPELESS = 19
        DXGI_FORMAT_D32_FLOAT_S8X24_UINT = 20
        DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS = 21
        DXGI_FORMAT_X32_TYPELESS_G8X24_UINT = 22
        DXGI_FORMAT_R10G10B10A2_TYPELESS = 23
        DXGI_FORMAT_R10G10B10A2_UNORM = 24
        DXGI_FORMAT_R10G10B10A2_UINT = 25
        DXGI_FORMAT_R11G11B10_FLOAT = 26
        DXGI_FORMAT_R8G8B8A8_TYPELESS = 27
        DXGI_FORMAT_R8G8B8A8_UNORM = 28
        DXGI_FORMAT_R8G8B8A8_UNORM_SRGB = 29
        DXGI_FORMAT_R8G8B8A8_UINT = 30
        DXGI_FORMAT_R8G8B8A8_SNORM = 31
        DXGI_FORMAT_R8G8B8A8_SINT = 32
        DXGI_FORMAT_R16G16_TYPELESS = 33
        DXGI_FORMAT_R16G16_FLOAT = 34
        DXGI_FORMAT_R16G16_UNORM = 35
        DXGI_FORMAT_R16G16_UINT = 36
        DXGI_FORMAT_R16G16_SNORM = 37
        DXGI_FORMAT_R16G16_SINT = 38
        DXGI_FORMAT_R32_TYPELESS = 39
        DXGI_FORMAT_D32_FLOAT = 40
        DXGI_FORMAT_R32_FLOAT = 41
        DXGI_FORMAT_R32_UINT = 42
        DXGI_FORMAT_R32_SINT = 43
        DXGI_FORMAT_R24G8_TYPELESS = 44
        DXGI_FORMAT_D24_UNORM_S8_UINT = 45
        DXGI_FORMAT_R24_UNORM_X8_TYPELESS = 46
        DXGI_FORMAT_X24_TYPELESS_G8_UINT = 47
        DXGI_FORMAT_R8G8_TYPELESS = 48
        DXGI_FORMAT_R8G8_UNORM = 49
        DXGI_FORMAT_R8G8_UINT = 50
        DXGI_FORMAT_R8G8_SNORM = 51
        DXGI_FORMAT_R8G8_SINT = 52
        DXGI_FORMAT_R16_TYPELESS = 53
        DXGI_FORMAT_R16_FLOAT = 54
        DXGI_FORMAT_D16_UNORM = 55
        DXGI_FORMAT_R16_UNORM = 56
        DXGI_FORMAT_R16_UINT = 57
        DXGI_FORMAT_R16_SNORM = 58
        DXGI_FORMAT_R16_SINT = 59
        DXGI_FORMAT_R8_TYPELESS = 60
        DXGI_FORMAT_R8_UNORM = 61
        DXGI_FORMAT_R8_UINT = 62
        DXGI_FORMAT_R8_SNORM = 63
        DXGI_FORMAT_R8_SINT = 64
        DXGI_FORMAT_A8_UNORM = 65
        DXGI_FORMAT_R1_UNORM = 66
        DXGI_FORMAT_R9G9B9E5_SHAREDEXP = 67
        DXGI_FORMAT_R8G8_B8G8_UNORM = 68
        DXGI_FORMAT_G8R8_G8B8_UNORM = 69
        DXGI_FORMAT_BC1_TYPELESS = 70
        DXGI_FORMAT_BC1_UNORM = 71
        DXGI_FORMAT_BC1_UNORM_SRGB = 72
        DXGI_FORMAT_BC2_TYPELESS = 73
        DXGI_FORMAT_BC2_UNORM = 74
        DXGI_FORMAT_BC2_UNORM_SRGB = 75
        DXGI_FORMAT_BC3_TYPELESS = 76
        DXGI_FORMAT_BC3_UNORM = 77
        DXGI_FORMAT_BC3_UNORM_SRGB = 78
        DXGI_FORMAT_BC4_TYPELESS = 79
        DXGI_FORMAT_BC4_UNORM = 80
        DXGI_FORMAT_BC4_SNORM = 81
        DXGI_FORMAT_BC5_TYPELESS = 82
        DXGI_FORMAT_BC5_UNORM = 83
        DXGI_FORMAT_BC5_SNORM = 84
        DXGI_FORMAT_B5G6R5_UNORM = 85
        DXGI_FORMAT_B5G5R5A1_UNORM = 86
        DXGI_FORMAT_B8G8R8A8_UNORM = 87
        DXGI_FORMAT_B8G8R8X8_UNORM = 88
        DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM = 89
        DXGI_FORMAT_B8G8R8A8_TYPELESS = 90
        DXGI_FORMAT_B8G8R8A8_UNORM_SRGB = 91
        DXGI_FORMAT_B8G8R8X8_TYPELESS = 92
        DXGI_FORMAT_B8G8R8X8_UNORM_SRGB = 93
        DXGI_FORMAT_BC6H_TYPELESS = 94
        DXGI_FORMAT_BC6H_UF16 = 95
        DXGI_FORMAT_BC6H_SF16 = 96
        DXGI_FORMAT_BC7_TYPELESS = 97
        DXGI_FORMAT_BC7_UNORM = 98
        DXGI_FORMAT_BC7_UNORM_SRGB = 99
        DXGI_FORMAT_AYUV = 100
        DXGI_FORMAT_Y410 = 101
        DXGI_FORMAT_Y416 = 102
        DXGI_FORMAT_NV12 = 103
        DXGI_FORMAT_P010 = 104
        DXGI_FORMAT_P016 = 105
        DXGI_FORMAT_420_OPAQUE = 106
        DXGI_FORMAT_YUY2 = 107
        DXGI_FORMAT_Y210 = 108
        DXGI_FORMAT_Y216 = 109
        DXGI_FORMAT_NV11 = 110
        DXGI_FORMAT_AI44 = 111
        DXGI_FORMAT_IA44 = 112
        DXGI_FORMAT_P8 = 113
        DXGI_FORMAT_A8P8 = 114
        DXGI_FORMAT_B4G4R4A4_UNORM = 115

        DXGI_FORMAT_P208 = 130
        DXGI_FORMAT_V208 = 131
        DXGI_FORMAT_V408 = 132

        DXGI_FORMAT_FORCE_UINT = (&HFFFFFFFF)
    End Enum

    Public Enum DXGI_MODE_SCANLINE_ORDER
        DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED = 0
        DXGI_MODE_SCANLINE_ORDER_PROGRESSIVE = 1
        DXGI_MODE_SCANLINE_ORDER_UPPER_FIELD_FIRST = 2
        DXGI_MODE_SCANLINE_ORDER_LOWER_FIELD_FIRST = 3
    End Enum

    Public Enum DXGI_MODE_SCALING
        DXGI_MODE_SCALING_UNSPECIFIED = 0
        DXGI_MODE_SCALING_CENTERED = 1
        DXGI_MODE_SCALING_STRETCHED = 2
    End Enum

    <ComImport>
    <Guid("aec22fb8-76f3-4639-9be0-28eb43a67a2e")>
    <InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>
    Interface IDXGIObject
        Function SetPrivateData(ByRef Name As Guid, ByVal DataSize As UInteger, ByVal pData As IntPtr) As HRESULT
        Function SetPrivateDataInterface(ByRef Name As Guid, ByVal pUnknown As IntPtr) As HRESULT
        Function GetPrivateData(ByRef Name As Guid, <Out> ByRef pDataSize As UInteger, <Out> ByRef pData As IntPtr) As HRESULT
        Function GetParent(ByRef riid As Guid, <Out> ByRef ppParent As IntPtr) As HRESULT
    End Interface

    <ComImport>
    <Guid("310d36a0-d2e7-4c0a-aa04-6a9d23b8886a")>
    <InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>
    Interface IDXGISwapChain
        Inherits IDXGIDeviceSubObject

        Overloads Function SetPrivateData(ByRef Name As Guid, ByVal DataSize As UInteger, ByVal pData As IntPtr) As HRESULT
        Overloads Function SetPrivateDataInterface(ByRef Name As Guid, ByVal pUnknown As IntPtr) As HRESULT
        Overloads Function GetPrivateData(ByRef Name As Guid, <Out> ByRef pDataSize As UInteger, <Out> ByRef pData As IntPtr) As HRESULT
        Overloads Function GetParent(ByRef riid As Guid, <Out> ByRef ppParent As IntPtr) As HRESULT
        Overloads Function GetDevice(ByRef riid As Guid, <Out> ByRef ppDevice As IntPtr) As HRESULT
        Function Present(ByVal SyncInterval As UInteger, ByVal Flags As UInteger) As HRESULT
        Function GetBuffer(ByVal Buffer As UInteger, ByRef riid As Guid, ByRef ppSurface As IntPtr) As HRESULT
        Function SetFullscreenState(ByVal Fullscreen As Boolean, ByVal pTarget As IDXGIOutput) As HRESULT
        Function GetFullscreenState(ByVal pFullscreen As Boolean, <Out> ByRef ppTarget As IDXGIOutput) As HRESULT
        Function GetDesc(<Out> ByRef pDesc As DXGI_SWAP_CHAIN_DESC) As HRESULT
        Function ResizeBuffers(ByVal BufferCount As UInteger, ByVal Width As UInteger, ByVal Height As UInteger, ByVal NewFormat As DXGI_FORMAT, ByVal SwapChainFlags As UInteger) As HRESULT
        Function ResizeTarget(ByVal pNewTargetParameters As DXGI_MODE_DESC) As HRESULT
        Function GetContainingOutput(<Out> ByRef ppOutput As IDXGIOutput) As HRESULT
        Function GetFrameStatistics(<Out> ByRef pStats As DXGI_FRAME_STATISTICS) As HRESULT
        Function GetLastPresentCount(<Out> ByRef pLastPresentCount As UInteger) As HRESULT
    End Interface

    <StructLayout(LayoutKind.Sequential)>
    Public Structure DXGI_FRAME_STATISTICS
        Public PresentCount As UInteger
        Public PresentRefreshCount As UInteger
        Public SyncRefreshCount As UInteger
        Public SyncQPCTime As LARGE_INTEGER
        Public SyncGPUTime As LARGE_INTEGER
    End Structure

    <StructLayout(LayoutKind.Explicit)>
    Public Structure LARGE_INTEGER
        <FieldOffset(0)>
        Public LowPart As Integer
        <FieldOffset(4)>
        Public HighPart As Integer
        <FieldOffset(0)>
        Public QuadPart As Long
    End Structure

    <ComImport>
    <Guid("2411e7e1-12ac-4ccf-bd14-9798e8534dc0")>
    <InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>
    Interface IDXGIAdapter
        Inherits IDXGIObject

        Overloads Function SetPrivateData(ByRef Name As Guid, ByVal DataSize As UInteger, ByVal pData As IntPtr) As HRESULT
        Overloads Function SetPrivateDataInterface(ByRef Name As Guid, ByVal pUnknown As IntPtr) As HRESULT
        Overloads Function GetPrivateData(ByRef Name As Guid, <Out> ByRef pDataSize As UInteger, <Out> ByRef pData As IntPtr) As HRESULT
        Overloads Function GetParent(ByRef riid As Guid, <Out> ByRef ppParent As IntPtr) As HRESULT
        <PreserveSig>
        Function EnumOutputs(ByVal Output As UInteger, ByRef ppOutput As IDXGIOutput) As HRESULT
        Function GetDesc(<Out> ByRef pDesc As DXGI_ADAPTER_DESC) As HRESULT
        Function CheckInterfaceSupport(ByRef InterfaceName As Guid, <Out> ByRef pUMDVersion As LARGE_INTEGER) As HRESULT
    End Interface

    <ComImport>
    <Guid("ae02eedb-c735-4690-8d52-5a8dc20213aa")>
    <InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>
    Interface IDXGIOutput
        Inherits IDXGIObject

        Overloads Function SetPrivateData(ByRef Name As Guid, ByVal DataSize As UInteger, ByVal pData As IntPtr) As HRESULT
        Overloads Function SetPrivateDataInterface(ByRef Name As Guid, ByVal pUnknown As IntPtr) As HRESULT
        Overloads Function GetPrivateData(ByRef Name As Guid, <Out> ByRef pDataSize As UInteger, <Out> ByRef pData As IntPtr) As HRESULT
        Overloads Function GetParent(ByRef riid As Guid, <Out> ByRef ppParent As IntPtr) As HRESULT
        Function GetDesc(<Out> ByRef pDesc As DXGI_OUTPUT_DESC) As HRESULT
        Function GetDisplayModeList(ByVal EnumFormat As DXGI_FORMAT, ByVal Flags As UInteger, ByRef pNumModes As UInteger, ByVal pDesc As DXGI_MODE_DESC) As HRESULT
        Function FindClosestMatchingMode(ByVal pModeToMatch As DXGI_MODE_DESC, <Out> ByRef pClosestMatch As DXGI_MODE_DESC, ByVal pConcernedDevice As IntPtr) As HRESULT
        Function WaitForVBlank() As HRESULT
        Function TakeOwnership(ByVal pDevice As IntPtr, ByVal Exclusive As Boolean) As HRESULT
        Sub ReleaseOwnership()
        Function GetGammaControlCapabilities(<Out> ByRef pGammaCaps As DXGI_GAMMA_CONTROL_CAPABILITIES) As HRESULT
        Function SetGammaControl(ByVal pArray As DXGI_GAMMA_CONTROL) As HRESULT
        Function GetGammaControl(<Out> ByRef pArray As DXGI_GAMMA_CONTROL) As HRESULT
        Function SetDisplaySurface(ByVal pScanoutSurface As IDXGISurface) As HRESULT
        Function GetDisplaySurfaceData(ByVal pDestination As IDXGISurface) As HRESULT
        Function GetFrameStatistics(<Out> ByRef pStats As DXGI_FRAME_STATISTICS) As HRESULT
    End Interface

    <StructLayout(LayoutKind.Sequential)>
    Public Structure DXGI_GAMMA_CONTROL_CAPABILITIES
        Public ScaleAndOffsetSupported As Boolean
        Public MaxConvertedValue As Single
        Public MinConvertedValue As Single
        Public NumGammaControlPoints As UInteger
        <MarshalAs(UnmanagedType.R4, SizeConst:=1025)>
        Private ControlPointPositions As Single
    End Structure

    <ComImport>
    <Guid("cafcb56c-6ac3-4889-bf47-9e23bbd260ec")>
    <InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>
    Interface IDXGISurface
        Inherits IDXGIDeviceSubObject

        Overloads Function SetPrivateData(ByRef Name As Guid, ByVal DataSize As UInteger, ByVal pData As IntPtr) As HRESULT
        Overloads Function SetPrivateDataInterface(ByRef Name As Guid, ByVal pUnknown As IntPtr) As HRESULT
        Overloads Function GetPrivateData(ByRef Name As Guid, <Out> ByRef pDataSize As UInteger, <Out> ByRef pData As IntPtr) As HRESULT
        Overloads Function GetParent(ByRef riid As Guid, <Out> ByRef ppParent As IntPtr) As HRESULT
        Overloads Function GetDevice(ByRef riid As Guid, <Out> ByRef ppDevice As IntPtr) As HRESULT
        Function GetDesc(<Out> ByRef pDesc As DXGI_SURFACE_DESC) As HRESULT
        Function Map(<Out> ByRef pLockedRect As DXGI_MAPPED_RECT, ByVal MapFlags As UInteger) As HRESULT
        Function Unmap() As HRESULT
    End Interface

    <StructLayout(LayoutKind.Sequential)>
    Public Structure DXGI_SURFACE_DESC
        Public Width As UInteger
        Public Height As UInteger
        Public Format As DXGI_FORMAT
        Public SampleDesc As DXGI_SAMPLE_DESC
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure DXGI_MAPPED_RECT
        Public Pitch As Integer
        Public pBits As IntPtr
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Unicode)>
    Public Structure DXGI_OUTPUT_DESC
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=32)>
        Public DeviceName As String
        Public DesktopCoordinates As RECT
        Public AttachedToDesktop As Boolean
        Public Rotation As DXGI_MODE_ROTATION
        Public Monitor As IntPtr
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure RECT
        Public left As Integer
        Public top As Integer
        Public right As Integer
        Public bottom As Integer

        Public Sub New(ByVal left As Integer, ByVal top As Integer, ByVal right As Integer, ByVal bottom As Integer)
            Me.left = left
            Me.top = top
            Me.right = right
            Me.bottom = bottom
        End Sub

        Public Shared Function FromXYWH(ByVal x As Integer, ByVal y As Integer, ByVal width As Integer, ByVal height As Integer) As RECT
            Return New RECT(x, y, x + width, y + height)
        End Function
    End Structure

    Public Enum DXGI_MODE_ROTATION
        DXGI_MODE_ROTATION_UNSPECIFIED = 0
        DXGI_MODE_ROTATION_IDENTITY = 1
        DXGI_MODE_ROTATION_ROTATE90 = 2
        DXGI_MODE_ROTATION_ROTATE180 = 3
        DXGI_MODE_ROTATION_ROTATE270 = 4
    End Enum

    <StructLayout(LayoutKind.Sequential)>
    Public Structure DXGI_GAMMA_CONTROL
        Public Scale As DXGI_RGB
        Public Offset As DXGI_RGB
        <MarshalAs(UnmanagedType.Struct, SizeConst:=1025)>
        Private GammaCurve As DXGI_RGB
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure DXGI_RGB
        Public Red As Single
        Public Green As Single
        Public Blue As Single
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Unicode)>
    Public Structure DXGI_ADAPTER_DESC1
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)>
        Public Description As String
        Public VendorId As UInteger
        Public DeviceId As UInteger
        Public SubSysId As UInteger
        Public Revision As UInteger
        Public DedicatedVideoMemory As UInteger
        Public DedicatedSystemMemory As UInteger
        Public SharedSystemMemory As UInteger
        Public AdapterLuid As LUID
        Public Flags As UInteger
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Unicode)>
    Public Structure DXGI_ADAPTER_DESC
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)>
        Public Description As String
        Public VendorId As UInteger
        Public DeviceId As UInteger
        Public SubSysId As UInteger
        Public Revision As UInteger
        Public DedicatedVideoMemory As UInteger
        Public DedicatedSystemMemory As UInteger
        Public SharedSystemMemory As UInteger
        Public AdapterLuid As LUID
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure LUID
        Private LowPart As UInteger
        Private HighPart As Integer
    End Structure

    <ComImport>
    <Guid("3d3e0379-f9de-4d58-bb6c-18d62992f1a6")>
    <InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>
    Interface IDXGIDeviceSubObject
        Inherits IDXGIObject

        Overloads Function SetPrivateData(ByRef Name As Guid, ByVal DataSize As UInteger, ByVal pData As IntPtr) As HRESULT
        Overloads Function SetPrivateDataInterface(ByRef Name As Guid, ByVal pUnknown As IntPtr) As HRESULT
        Overloads Function GetPrivateData(ByRef Name As Guid, <Out> ByRef pDataSize As UInteger, <Out> ByRef pData As IntPtr) As HRESULT
        Overloads Function GetParent(ByRef riid As Guid, <Out> ByRef ppParent As IntPtr) As HRESULT
        Function GetDevice(ByRef riid As Guid, <Out> ByRef ppDevice As IntPtr) As HRESULT
    End Interface


    <DllImport("DXGI.dll", SetLastError:=True, CharSet:=CharSet.Unicode)>
    Public Shared Function CreateDXGIFactory1(ByRef riid As Guid, <Out> ByRef ppFactory As IDXGIFactory1) As HRESULT
    End Function

    Public Const DXGI_ERROR_NOT_FOUND As Long = &H887A0002
